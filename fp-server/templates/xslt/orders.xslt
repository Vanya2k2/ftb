<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <div>
            <h3>Stats</h3>
                <!-- //student[count(. |  key('students-by-group', @group)[1] ) = 1 -->
                <!-- //student[@group=9] -->
                <h4>Total graphs:<xsl:value-of select="count(//graph)"/></h4>

                <xsl:apply-templates select="//graph"/>
        </div>
    </xsl:template>

    <xsl:template match="graph">
        <h2>owner: <xsl:apply-templates select="@owner"/></h2>
    </xsl:template>



</xsl:stylesheet>