/* tslint:disable */
/* eslint-disable */
/**
 * FP Graphs
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.4
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface Order
 */
export interface Order {
    /**
     * 
     * @type {string}
     * @memberof Order
     */
    order?: string;
    /**
     * 
     * @type {string}
     * @memberof Order
     */
    date?: string;
    /**
     * 
     * @type {string}
     * @memberof Order
     */
    product?: string;
    /**
     * 
     * @type {string}
     * @memberof Order
     */
    category?: string;
}

export function OrderFromJSON(json: any): Order {
    return OrderFromJSONTyped(json, false);
}

export function OrderFromJSONTyped(json: any, ignoreDiscriminator: boolean): Order {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'order': !exists(json, 'order') ? undefined : json['order'],
        'date': !exists(json, 'date') ? undefined : json['date'],
        'product': !exists(json, 'product') ? undefined : json['product'],
        'category': !exists(json, 'category') ? undefined : json['category'],
    };
}

export function OrderToJSON(value?: Order | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'order': value.order,
        'date': value.date,
        'product': value.product,
        'category': value.category,
    };
}


