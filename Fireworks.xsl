<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xls="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <style>
                * {
                font-size: 16px;
                }
                h1 {
                font-size: 20px;
                }
                h2 {
                font-size: 18px;
                }
            </style>
            <body>
                <h1>Fireworks</h1>
                <p><b>Total runs:</b> <xsl:value-of select="count(//run)"/></p>
                <xsl:apply-templates select="//runs"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="//runs">
        <h2>Run information</h2>
        <xsl:apply-templates select="//run"/>
    </xsl:template>

    <xsl:template match="run">
        <span><pre>Run ID: <xsl:value-of select="generate-id(.)"/>  Watchers: <xsl:value-of select="@watchers"/>  Date: <xsl:value-of select="@date"/>  Time: <xsl:value-of select="@time"/></pre></span>
        <h3>Participants</h3>
        <h3>Number of participants: <xsl:value-of select="count(//member)"/></h3>
        <span><ul><xsl:apply-templates select="//member"/></ul></span>
    </xsl:template>

    <xsl:template match="//member">
        <li><pre>Name: <xsl:value-of select="@name"/>  Age: <xsl:value-of select="@age"/>  Number: <xsl:value-of select="@number"/></pre></li>
    </xsl:template>

</xsl:stylesheet>